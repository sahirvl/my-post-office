const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const UserModel = require("../models/users");
const { config } = require("../configs/config");

async function login(req, res) {
  const { userName, password } = req.body;

  // 1. Buscar usuario
  try {
    const user = await UserModel.findOne({ userName });
    if (user) {
      // SI LAS CREDENCIALES SON VALIDAS
      if (bcrypt.compare(password, user.password)) {
        // CREAMOS Y ENVIAMOS EL TOKEN
        const payload = {
          sub: user.id,
          user: user.userName,
          role: user.role,
        };
        jwt.sign(payload, config.jwtSecret, (err, token) => {
          if (err) res.status(500).send({ message: "Error" });
          else res.send({ auth: true, payload, token });
        });
      }
    } else res.status(400).send({ message: "Please check your credentials" });
  } catch (err) {
    res.status(500).send({ message: "Internal server error" });
  }
}

function validateJWT(req, res) {
  res.status(200).json({ auth: true });
}

module.exports = { login, validateJWT };
