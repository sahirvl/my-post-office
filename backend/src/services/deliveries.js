const CorrespondenceModel = require("../models/correspondence");
const DeliveriesModel = require("../models/deliveries");

/*Este servicio al entregar un documento de correo crea un documento con el id del correo entregado,
el id de el user que lo entrego y a su vez actualiza el estado del documento el cual por defecto es-
pending, este lo actualiza a delivered*/

// CREATE DELIVERED DOCUMENT

async function deliveredMail(req, res) {
  const body = req.body;
  try {
    const delivered = await DeliveriesModel.create(body);
    const updateStatus = await CorrespondenceModel.findById(
      delivered.correspondenceId
    );
    await updateStatus.updateOne({ status: "delivered" });
    return res.status(201).send(delivered);
  } catch (error) {
    return res.status(500).send({ message: "Something gone wrong" });
  }
}

// SHOW ALL DOCUMENTS DELIVERED

async function getDelivered(req, res) {
  try {
    // aprendi a popular un campo para que no salga solo el ObjetId
    // y manipular los campos que quiero mostrar, para proteger datos sensibles.
    const deliveredList = await DeliveriesModel.find({})
      .sort({ dateTime: -1 })
      .populate("correspondenceId") // aqui llamamos metodo populate para popular esta info
      .populate({
        path: "deliveredBy",
        select: ["-password", "-role", "-__v"],
      })
      .exec();

    if (!deliveredList) {
      return res.status(404).send({ message: "Empty data" });
    } else {
      return res.status(200).send(deliveredList);
    }
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).send({ message: "Internal server error" });
  }
}

module.exports = { deliveredMail, getDelivered };
