const UserModel = require("../models/users");
const { hashSync, genSaltSync } = require("bcrypt");

/*
Este es un metodo CRUD sencillo para los usuarios, en donde se agregara un middleware
para que unicamente los usuarios con rol de admin pueda tener privilegios de crear,buscar,actualizar o eliminar
los usuarios creados.
*/

// CREATE USER

async function createUser(req, res) {
  const body = req.body;
  try {
    const newUser = await UserModel.create(body);
    return res.status(201).send(newUser);
  } catch (error) {
    return res.status(500).send({message: "Cannot create user, try again"});
  }
}

// GET ALL USERS

async function getUsers(req, res) {
  try {
    // en el metodo find() puedo pasar como parametro un array con los campos que deseo ocultar.
    const userList = await UserModel.find({}, ['-password', '-__v']).sort({ userName: 1 });
    return res.send(userList);
  } catch (error) {
    return res.status(400).send({ message: "Bad request" });
  }
}

// GET USER BY ID

async function getUserId(req, res) {
  const { id } = req.params;
  try {
    const user = await UserModel.findById({ _id: id }, ['-password', '-__v']);
    return res.send(user);
  } catch (error) {
    return res.status(400).send({ mensaje: "Bad request" });
  }
}

// UPDATE USER

async function updateUser(req, res) {
  const { id } = req.params;
  try {
    const user = await UserModel.findById(id);
    if (id) {
      const hashpassword = hashSync(user.password, genSaltSync());
      req.body.password = hashpassword;
      await user.updateOne(req.body);
      return res.send(
        `${user.userName}, update succesfully, please refresh this page to see changes`
      );
    }
  } catch (error) {
    return res.send({ mensaje: "Something gone wrong" });
  }
}

// DELETE USER

async function deleteUser(req, res) {
  const { id } = req.query;
  try {
    const user = await UserModel.findByIdAndDelete({ _id: id });
    return res.send(`${user.userName} deleted.`);
  } catch (error) {
    return res.send({ mensaje: "Not found" });
  }
}

module.exports = { createUser, getUsers, getUserId, deleteUser, updateUser };
