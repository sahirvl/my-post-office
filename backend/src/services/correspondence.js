const CorrespondenceModel = require("../models/correspondence");

/*
User con rol de employee puede crear nuevo documento, ver los documentos en inbox y filtrar por siertos parametros,
y el rol de admin puede hacer lo anterior, ademas de actualizar o eliminar documentos.
*/

// CREATE NEW MAIL
async function createMail(req, res) {
  const body = req.body;
  try {
    const newMail = await CorrespondenceModel.create(body);
    return res.status(201).send(newMail);
  } catch (error) {
    return res.status(500).send({ message: "Something gone wrong" });
  }
}

// SHOW ONLY DOCUMENTS PENDING SORT BY DATE DESC
async function getInbox(req, res) {
  try {
    const mailList = await CorrespondenceModel.find({ status: "pending" }).sort(
      { dateTime: -1 }
    );
    return res.send(mailList);
  } catch (error) {
    return res.send({ message: "Empty list" });
  }
}

// SHOW 3 STATUS PENDING, DELIVERED OR RETURNED
async function getFilter(req, res) {
  const { status, block, apartament } = req.query;
  try {
    const mail = await CorrespondenceModel.find({
      $or: [{ status }, { block }, { apartament }],
    });
    return res.send(mail);
  } catch (error) {
    return res.send({ message: "Empty list" });
  }
}

// UPDATE CORRESPONDENCE

async function updateMail(req, res) {
  const { id } = req.params;
  try {
    const mail = await CorrespondenceModel.findById(id);
    if (id) {
      await mail.updateOne(req.body);
      return res.send(
        `${mail}, update succesfully, please refresh this page to see changes`
      );
    }
  } catch (error) {
    return res.send({ message: "Something gone wrong" });
  }
}

// DELETE CORRESPONDENCE

async function deleteMail(req, res) {
  const { id } = req.query;
  try {
    const mail = await CorrespondenceModel.findByIdAndDelete({ _id: id });
    return res.send(`The mail ${mail.id} was deleted successfully`);
  } catch (error) {
    return res.send({ message: "Not found" });
  }
}

module.exports = { createMail, getInbox, getFilter, updateMail, deleteMail };
