const { Router } = require("express");
const { deliveredMail, getDelivered } = require("../services/deliveries");
const { validToken, roleCheck } = require("../middlewares/auth_handler");

const routerDelivered = Router();

routerDelivered.post("", [validToken, roleCheck], deliveredMail);
routerDelivered.get("/list", [validToken, roleCheck], getDelivered);

module.exports = routerDelivered;
