const { Router } = require("express");
const {
  createUser,
  getUserId,
  getUsers,
  deleteUser,
  updateUser,
} = require("../services/users");
const { validToken, adminCheck } = require("../middlewares/auth_handler");

const routerUser = Router();

routerUser.get("/list", [validToken, adminCheck], getUsers);
routerUser.post("", [validToken, adminCheck], createUser);
routerUser.get("/:id", [validToken, adminCheck], getUserId);
routerUser.put("/update/:id", [validToken, adminCheck], updateUser);
routerUser.delete("", [validToken, adminCheck], deleteUser);

module.exports = routerUser;
