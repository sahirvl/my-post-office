const { Router } = require("express");
const {
  createMail,
  getInbox,
  getFilter,
  updateMail,
  deleteMail,
} = require("../services/correspondence");
const {
  validToken,
  roleCheck,
  adminCheck,
} = require("../middlewares/auth_handler");

const routerMail = Router();
// aqui las rutas con middlewares que validan token y rol
routerMail.post("/new-doc", [validToken, roleCheck], createMail);
routerMail.get("/inbox", [validToken, roleCheck], getInbox);
routerMail.get("", [validToken, roleCheck], getFilter);
routerMail.put("/update/:id", [validToken, adminCheck], updateMail);
routerMail.delete("/delete", [validToken, adminCheck], deleteMail);

module.exports = routerMail;
