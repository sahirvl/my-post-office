const { Router } = require("express");
const { login, validateJWT } = require("../services/auth_login");
const { validToken } = require("../middlewares/auth_handler");

const routerAuth = Router();

routerAuth.post("/login", login);
routerAuth.get("/verify", [validToken], validateJWT);

module.exports = routerAuth;
