const jwt = require("jsonwebtoken");
const { config } = require("../configs/config");

function validToken(req, res, next) {
  const authHeader = req.header("Authorization");

  if (!authHeader) {
    // Si no se proporciona ningún encabezado de autorización
    return res.status(401).send({ message: "Empty token" });
  }

  if (authHeader && authHeader.startsWith("Bearer ")) {
    // Aquí utilizo el método split para separar el JWT
    const token = authHeader.split(" ")[1];

    try {
      if (token) {
        const decodedToken = jwt.verify(token, config.jwtSecret);
        req.user = decodedToken;
        next();
      } else {
        res.status(401).send({ message: "Invalid token" });
      }
    } catch (error) {
      res.status(500).send({ message: "Internal server error" });
    }
  } else {
    res.status(401).send({ message: "invalid authorization header" });
  }
}

function adminCheck(req, res, next) {
  const user = req.user;
  try {
    user.role === "admin"
      ? next()
      : res
          .status(403)
          .json({ message: "You must have administrator privilegies" });
  } catch (error) {
    res.status(500).send({ message: "Internal Server Error" });
  }
}

function roleCheck(req, res, next) {
  const user = req.user;
  try {
    user.role === "admin" || "employee"
      ? next()
      : res.status(403).json({ message: "You must have some privilegies" });
  } catch (error) {
    res.status(500).send({ message: "Internal Server Error" });
  }
}

module.exports = { validToken, adminCheck, roleCheck };
