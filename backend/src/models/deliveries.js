const { Schema, model } = require("mongoose");

const deliverieSchema = new Schema({
  correspondenceId: {
    type: Schema.Types.ObjectId,
    ref: "Correspondence",
    required: true,
  },
  deliveredBy: {
    type: Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  receivedBy: {
    type: String,
    required: true,
  },
  dateTime: {
    type: Date,
    default: Date.now,
  },
});

const DeliveriesModel = model("Deliveries", deliverieSchema);

module.exports = DeliveriesModel;
