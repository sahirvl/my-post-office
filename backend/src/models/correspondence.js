const { Schema, model } = require("mongoose");

const CorrespondenceSchema = new Schema({
  block: {
    type: Number,
    required: [true, "CHOISE THE BLOCK 1,2 OR 3"],
    enum: [1, 2, 3],
  },
  apartament: {
    type: Number,
    required: [true, "PLEASE ENTER THE APARTAMENT NUMBER"],
  },
  dateTime: {
    type: Date,
    default: Date.now,
  },
  mailCompany: {
    type: String,
    required: [true, "PLEASE ENTER THE MAIL COMPANY"],
  },
  from: {
    type: String,
    required: [true, "FROM ?"],
  },
  description: {
    type: String,
    required: [true, "WRITE A SHORT DESCRIPTION"],
    minlength: 10,
  },
  status: {
    type: String,
    enum: ["delivered", "pending", "returned"],
    default: "pending",
  },
});

const CorrespondenceModel = model("Correspondence", CorrespondenceSchema);

module.exports = CorrespondenceModel;
