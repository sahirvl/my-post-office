const { Schema, model } = require("mongoose");
const bcrypt = require("bcrypt");

// definimos el schema

const userSchema = new Schema({
  userName: {
    type: String,
    required: [true, "ENTER A USERNAME"],
  },
  password: {
    type: String,
    required: [true, "CHOOSE YOUR SECURITY PASSWORD"],
  },
  role: {
    type: String,
    required: [true, "CHOOSE A ROLE ADMIN OR EMPLOYEE"],
    enum: {
      values: ["admin", "employee"],
      message: 'El rol debe ser "admin" o "employee".',
    },
  },
});
// Middleware para hashear la contraseña antes de guardar

userSchema.pre("save", async function (next) {
  if (this.isModified("password")) {
    this.password = await bcrypt.hash(this.password, 10);
  }
  next();
});

const UserModel = model("user", userSchema);

module.exports = UserModel;
