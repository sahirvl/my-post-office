const mongoose = require("mongoose");
const { config } = require("./config");

// 1. Conectarse

const conexionDB = () => {
  mongoose
    .connect(config.dbUrlDev, { useNewUrlParser: true })
    .then(() => {
      console.log("DB CONNECTION SUCCESSFUL ✅");
    })
    .catch(() => {
      console.log("DB CONNECTION LOST 💔");
    });
};

module.exports = conexionDB;
