require("dotenv").config();

const config = {
  port: process.env.PORT,
  dbUrlDev: process.env.URL_DATABASE_DEV,
  jwtSecret: process.env.JWT_SECRET,
};

module.exports = { config };
