const express = require("express");
const connectDB = require("./database");
const routerUser = require("../routes/user");
const routerMail = require("../routes/correspondence");
const { config } = require("./config");
const routerDelivered = require("../routes/deliveries");
const { logErrors, errorHandler } = require("../middlewares/error_handler");

const PORT = config.port;

class Server {
  constructor() {
    this.app = express();

    // Middlewares
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: false }));

    this.app.listen(PORT, () => {
      console.log("SERVER ONLINE ✅");
    });

    this.rutas();

    this.app.use(logErrors);
    this.app.use(errorHandler);

    connectDB();
  }

  rutas() {
    this.app.get("/", (req, res) => {
      res.send("MY SERVER 🚀✅");
    });
    this.app.use("/user", routerUser);
    this.app.use("/mail", routerMail);
    this.app.use("/deliveries", routerDelivered);
    this.app.use("/auth", require("../routes/auth"));
  }
}

module.exports = Server;
