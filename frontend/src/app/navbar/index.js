import "./navbar.css";
import { RiInboxArchiveLine, RiMenuFill, RiLogoutBoxRLine } from "react-icons/ri";


export default function Navbar() {
  return (
    <nav>
      <ul>
        <li>
        <a href="#">Cerrar Sesión <RiLogoutBoxRLine /></a>

        </li>
        <li>
          <a href="#">Configuración <RiMenuFill /></a>
        </li>
        <li>
        <a href="#">Inicio <RiInboxArchiveLine /></a>
        </li>
      </ul>
    </nav>
  );
}
