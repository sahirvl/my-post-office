import ContentMailTable from "./contentMailTable";
import Navbar from "./navbar";

const MAILS = [
  {
    id: 1,
    block: "5",
    apartment: "1501",
    dateTime: "27/11/2023 11:45",
    mailCompany: "Mail Company",
    from: "Camila Cuervo",
    description: "sobre sellado con documentos",
    status: "pending",
  },
  {
    id: 2,
    block: "2",
    apartment: "1302",
    dateTime: "27/11/2023 11:45",
    mailCompany: "Mail Company",
    from: "Oscar Segura",
    description: "sobre sellado con documentos",
    status: "pending",
  },
  {
    id: 3,
    block: "2",
    apartment: "1305",
    dateTime: "27/11/2023 14:32",
    mailCompany: "TCC",
    from: "Carlos Hook",
    description: "sobre sellado con documentos",
    status: "delivered",
  },
  {
    id: 4,
    block: "1",
    apartment: "203",
    dateTime: "29/11/2023 14:50",
    mailCompany: "React",
    from: "Jhon Doe",
    description: "Libro aprender a programar con React",
    status: "pending",
  },
  {
    id: 5,
    block: "1",
    apartment: "204",
    dateTime: "01/12/2023 14:03",
    mailCompany: "TCC",
    from: "Jhon Doe",
    description: "Se devuelve porque no existe el destinatario",
    status: "returned",
  },
];

export default function Home() {
  
  return (
    <>
      <h1>MI PRIMER PROYECTO CON REACT EN CONSTRUCCION...</h1>
      <Navbar />
      <ContentMailTable mails={MAILS} />
    </>
  );
}
