'use client'
import { useState } from "react";
import SearchBar from "./searchBar";
import MailTable from "../contentMailTable/mailTable";
import AddMailBtn from "./addMailBtn";
import "./contentMailTable.css"


export default function ContentMailTable({ mails }) {
  const [filterText, setFilterText] = useState('');
  const [pendingOnly, setPendingOnly] = useState(false);



  return (
    <div className="content">
      <SearchBar filterText={filterText} 
                 pendingOnly={pendingOnly} 
                 onFilterTextChange={setFilterText}
                 onPendingOnlyChange={setPendingOnly}/>
      <MailTable mails={mails}
                 filterText={filterText} 
                 pendingOnly={pendingOnly} />
      <AddMailBtn />
    </div>
  )
}
