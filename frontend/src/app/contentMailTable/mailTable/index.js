import MailRow from "../mailRow";
import "./mailTable.css"


export default function MailTable({ mails, filterText, pendingOnly }) {
  const rows = [];

  mails.forEach((mail) => {
    if (
      mail.apartment.toLowerCase().indexOf(
        filterText.toLowerCase()
      ) === -1
    ) {
      return;
    }
    if (pendingOnly && mail.status !== 'pending') {
      return;
    }
    rows.push(
      <MailRow
        mail={mail}
        key={mail.id} />
    )

  })

  return (
    <table>
      <thead>
        <tr>
          <th>Bloque</th>
          <th>Apartamento</th>
          <th>Fecha/Hora</th>
          <th>Mensajero</th>
          <th>Quien envia</th>
          <th>Descripcion</th>
          <th>Estado</th>
        </tr>
      </thead>
      <tbody>{rows}</tbody>
    </table>
  )
}
