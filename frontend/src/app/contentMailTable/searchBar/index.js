import "./searchbar.css"

export default function SearchBar({filterText,pendingOnly,onFilterTextChange,
  onPendingOnlyChange}) {


  return (
    <form>
      <input type="text" 
        value={filterText} placeholder="Buscar..." 
        onChange={(e) => onFilterTextChange(e.target.value)} />
      <label>
        <input type="checkbox" 
          checked={pendingOnly} 
          onChange={(e) => onPendingOnlyChange(e.target.checked)} />
        {' '}
        Mostrar solo pendientes
      </label>
    </form>
  );
}
