import "./mailRow.css";

export default function MailRow({ mail }) {
  let statusStyle;

  if (mail.status === "pending") {
    statusStyle = { color: "purple" };
  } else if (mail.status === "delivered") {
    statusStyle = { color: "green" };
  } else {
    statusStyle = { background: "red" };
  }

  return (
    <tr>
      <td>{mail.block}</td>
      <td>{mail.apartment}</td>
      <td>{mail.dateTime}</td>
      <td>{mail.mailCompany}</td>
      <td>{mail.from}</td>
      <td>{mail.description}</td>
      <td>
        <span style={statusStyle}>{mail.status.toUpperCase()}</span>
      </td>
    </tr>
  );
}
